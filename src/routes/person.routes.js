import {Router} from 'express';
import {index, store, find, destroy, update} from "../controllers/person.controller";

const router = Router();

router.route('/')
    .get(index)
    .post(store);

router.route('/:id')
    .get(find)
    .put(update)
    .delete(destroy);

export default router;
