import index from './../routes/index.routes';
import person from './../routes/person.routes';

const routes = async (app) => {
    app.use(index);
    app.use('/api/people', person);
};

export default routes;

