import Sequelize from 'sequelize';
import {config} from "dotenv";

// Dotenv
config();

export const DB = new Sequelize(
    process.env.DB_DATABASE,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD,
    {
        host: process.env.DB_HOST,
        dialect: process.env.DB_CONNECTION,
        pool: {
            max: 5,
            min: 0,
            require: 30000,
            idle: 10000
        },
        // logging: false
    }
);
