import express, {json} from 'express';
import cors from 'cors';
import morgan from 'morgan';
import './database';

const app = express();

// Global
app.set('port', process.env.PORT || 3000);

// Middlewares
app.use(morgan('dev'));
app.use(json());
app.use(cors());

export default app;
