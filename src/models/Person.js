import Sequelize from "sequelize";
import {DB} from "../config/database";

const Person = DB.define('person', {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: Sequelize.INTEGER
    },
    fullname: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            notEmpty: {
                msg: "El nombre es obligatorio."
            },
        }
    },
    birth: {
        type: Sequelize.DATE,
        allowNull: false,
        validate: {
            notEmpty: {
                msg: "La fecha de nacimiento es obligatoria."
            },
            isDate: true
        }
    },
    father_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
            model: 'user',
            key: 'id'
        }
    },
    mother_id: {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
            model: 'user',
            key: 'id'
        }
    }
}, {
    timestamps: false,
});

Person.hasOne(Person, {
    foreignKey: {
        name: 'father_id',
        allowNull: true
    },
});

Person.hasOne(Person, {
    foreignKey: {
        name: 'mother_id',
        allowNull: true
    },
});


export default Person;
