import Person from '../models/Person';

export const index = async (req, res) => {
    try {
        return res.json(await Person.findAll());
    } catch (e) {
        return res.status(400).json(e);
    }
};

export const store = async ({body: {fullname, birth, father_id = null, mother_id = null}}, res) => {
    try {

        const newPerson = await Person.create({
            fullname,
            birth,
            father_id,
            mother_id,
        });

        return res.status(201).json({
            message: "Created successfully.",
            person: newPerson
        });
    } catch (e) {
        return res.status(404).json(e);
    }
};

export const find = async ({params: {id}}, res) => {
    try {
        const person = await Person.findOne({
            where: {
                id
            }
        });

        return res.json(person);
    } catch (e) {
        return res.status(400).json(e);
    }
};

export const update = async ({params: {id}, body: {fullname, birth, father_id = null, mother_id = null}}, res) => {
    try {

        const personUpdate = await Person.findOne({
            where: {
                id
            }
        });

        if (fullname) personUpdate.fullname = fullname;
        if (birth) personUpdate.birth = birth;
        personUpdate.father_id = father_id;
        personUpdate.mother_id = mother_id;

        personUpdate.save();

        return res.json({
            message: "Updated successfully.",
            person: personUpdate
        });
    } catch (e) {
        return res.status(400).json(e);
    }
};

export const destroy = async ({params: {id}}, res) => {
    try {
        const deleteCount = await Person.destroy({
            where: {
                id
            }
        });

        if (deleteCount > 0)
            return res.json({
                message: "Deleted successfully."
            });

        return res.status(400).json({
            message: "Person not exists."
        });
    } catch (e) {
        return res.status(400).json(e);
    }
};
