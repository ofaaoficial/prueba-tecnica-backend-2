# Prueba Técnica
Solución de la prueba técnica.
## Rutas API

### Personas
| Función | Método | URL | 
| :---: | :---: | :---: | 
| Crear | POST | `/api/people` | 
| Listar | GET | `/api/people` |
| Buscar | GET | `/api/people/:id` | 
| Actualizar | PUT | `/api/people/:id` | 
| Borrar | DELETE | `/api/people/:id` | 

## Instalación
* Importar base de datos ubicada en la carpeta `db-dump/practice.sql`.
* Tener previamente instalado de `Nodejs` para la ejecución del proyecto y `npm` para la instalación de dependencias.

Para ejecutar localmente el proyecto, ejecute el siguiente comando:
```javascript
// Instalación de dependencias
$ npm install 

// Ejecución de proyecto 
$ npm run dev 
```

Para crear el build:
```javascript 
$ npm run build
```
Esto te creara un `build` para subirlo y ejecutarlo en un servidor. 


## Licencia 🔥
Copyright © 2020-present [Oscar Amado](https://github.com/ofaaoficial) 🧔
